
import StyleButton from "./components/StyledButton";
import { PrimaryButton, PropsButton } from "./components/PropsButton";
import {ExtendedButton, SuperButton} from "./components/ExtendedButton";

function App() {
  return (
    <div>
      <div>
        <StyleButton>I am a styled button</StyleButton>
      </div>
      <div>
        <PropsButton backColor="#ffc3c3" fontColor="#bf576b">Button 1</PropsButton>
        <PropsButton backColor="#FFDACA" fontColor="#DA6025">Button 2</PropsButton>
        <PropsButton backColor="#FFF4C7" fontColor="#D1A427">Button 3</PropsButton>
        <PropsButton>Button 4</PropsButton>
      </div>
      <div>
        <PrimaryButton primary>Primary Button</PrimaryButton>
        <PrimaryButton>Other Button 1</PrimaryButton>
        <PrimaryButton>Other Button 2</PrimaryButton>
      </div>
      <div>
        <SuperButton>Super</SuperButton>
        <ExtendedButton>Hello 1</ExtendedButton>
        <ExtendedButton backColor="#FFDACA" fontColor="#DA6025">Hello 2</ExtendedButton>
        <ExtendedButton backColor="#FFF4C7" fontColor="#D1A427">Hello 3</ExtendedButton>
      </div>
    </div>

  );
}

export default App;
