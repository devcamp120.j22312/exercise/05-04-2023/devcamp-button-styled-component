import styled from 'styled-components';

const SuperButton = styled.button`
background-color: #59B9C1;
color: white;
margin: 10px;
padding: pad;
padding: 10px;
border-radius: 10px;
border:  none;
`
const ExtendedButton = styled(SuperButton)`
background-color: ${props => props.backColor || "blue"};
color: ${props => props.fontColor || "cyan"};
`

export {ExtendedButton, SuperButton} ;