import styled from 'styled-components'

const PropsButton = styled.button`
background-color: ${props => props.backColor || "blue"};
color: ${props => props.fontColor || "cyan"};
margin: 10px;
padding: pad;
padding: 10px;
border-radius: 10px;
border:  none;
`

const PrimaryButton = styled.button`
background-color: ${props => props.primary ? "#59B9C1" : "pink"};
color: ${props => props.primary ? "white" : "orange"};
margin: 10px;
padding: pad;
padding: 10px;
border-radius: 10px;
border:  none;
`

export { PropsButton, PrimaryButton }